terraform {
  required_providers {
    mycloud = {
      source  = "hashicorp/aws"
      version = "~> 2.7"
    }
  }
}