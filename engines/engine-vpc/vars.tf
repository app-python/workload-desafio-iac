
variable "AWS_REGION" {
}

variable "CIDRVPC" {
}

variable "requerente" {

}

variable "map_public_true" {
}


variable "main_DMZ_A" {
}

variable "CIDR_main_DMZ_A" {
}


variable "main_DMZ_B" {
}

variable "CIDR_main_DMZ_B" {
}

variable "main_DMZ_C" {
}

variable "CIDR_main_DMZ_C" {
}

variable "main_DMZ_D" {
}

variable "CIDR_main_DMZ_D" {
}

variable "main_APP_A" {
}

variable "CIDR_main_APP_A" {
}

variable "main_APP_B" {
}

variable "CIDR_main_APP_B" {
}

variable "main_APP_C" {
}

variable "CIDR_main_APP_C" {
}

variable "main_APP_D" {
}

variable "CIDR_main_APP_D" {
}

variable "instance_tenancy" {
}

variable "enable_dns_support" {
}

variable "enable_dns_hostnames" {
}

variable "enable_classiclink" {
}

variable "name_vpc" {
}

variable "env" {
}

variable "cluster-name" {}

