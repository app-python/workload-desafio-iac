
resource "aws_ecr_repository_policy" "repo_policy" {
  count      = length(var.list_of_images)
  repository = element(aws_ecr_repository.images.*.id, count.index)
  policy     = <<POLICY
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "repo_policy",
            "Effect": "Allow",
            "Principal": {
              "AWS": [
                  "${var.list_account}"
              ]
            },
            "Action": [
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeRepositories",
                "ecr:GetRepositoryPolicy",
                "ecr:ListImages",
                "ecr:DeleteRepository",
                "ecr:BatchDeleteImage",
                "ecr:SetRepositoryPolicy",
                "ecr:DeleteRepositoryPolicy"
            ]
        }
    ]
}
POLICY
}

resource "aws_ecr_lifecycle_policy" "foopolicy-0" {
  count      = length(var.list_of_images)
  repository = element(aws_ecr_repository.images.*.id, count.index)
  policy     = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Keep last 30 images",
            "selection": {
                "tagStatus": "tagged",
                "tagPrefixList": ["v"],
                "countType": "imageCountMoreThan",
                "countNumber": 30
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}