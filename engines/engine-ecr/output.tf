output ecr-name {
  value = ["${aws_ecr_repository.images.*.name}",
    "${aws_ecr_repository.images.*.repository_url}"
  ]
}