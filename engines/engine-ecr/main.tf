resource "aws_ecr_repository" "images" {
  depends_on           = ["aws_iam_role_policy.ecr_admin_policy", "aws_iam_role.ecr_admin_role"]
  count                = length(var.list_of_images)
  name                 = element(var.list_of_images, count.index)
  image_tag_mutability = var.mutable
  image_scanning_configuration {
    scan_on_push = var.push
  }
}