terraform {
  backend "s3" {
    bucket  = "iac-be-develop-testes-terraform-rapha"
    encrypt = true
    key     = "desafio/esx/data.tfstate"
    region  = "us-east-1"
  }
}