data aws_vpc vpc {
  filter {
    name   = "tag:Name"
    values = ["eksvpc"]
  }
}

data aws_subnet_ids private {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["main_APP_*"]
  }
}

data aws_subnet_ids pub {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["main_DMZ_*"]
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

output vpcs {
    value = [
        data.aws_vpc.vpc.id,
        data.aws_subnet_ids.private,
        data.aws_vpc.vpc.cidr_block,
        data.aws_availability_zones.available.names[0],
        data.aws_availability_zones.available.names[1],
        data.aws_availability_zones.available.names[2],
        data.aws_availability_zones.available.names[3]

    ]
}



