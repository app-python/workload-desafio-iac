variable "ecr_produto" {
    type = string
    description = "Nome da BU, ou produto entregue"
}

variable "aws_region" {
    type = string
    description = "Região da conta"
}

variable "env" {
    type = string
    description = "Environment"
}

variable "push" {
    type = string
    description = "validação da imagem criada."
}

variable "mutable" {
    type = string
    description = "se imagem o tag será mutávelß"
}

variable "list_account" {
    type = string

}

variable "list_of_images" {
    type = list(string)
    description = "lista de contas"
}