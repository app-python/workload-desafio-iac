variable "AWS_REGION" {
  type        = string
  description = "Reigiao aws"
}

variable "CIDRVPC" {
  type        = string
  description = "Range de rede da vpc"
}

variable "requerente" {
  type        = string
  description = "Projeto que requeriu criacao"
}

variable "map_public_true" {
  type        = bool
  description = "Forca ip publico em subnet"
}

variable "main_DMZ_A" {
  type        = string
  description = "az de região A publica"
}

variable "CIDR_main_DMZ_A" {
  type        = string
  description = "range de rede subnet A publica"
}

variable "main_DMZ_B" {
  type        = string
  description = "az de região B publica"
}

variable "CIDR_main_DMZ_B" {
  type        = string
  description = "range de rede subnet b publica"
}

variable "main_DMZ_C" {
  type        = string
  description = "az de região C publica"
}

variable "CIDR_main_DMZ_C" {
  type        = string
  description = "range de rede subnet C publica"
}

variable "main_DMZ_D" {
  type        = string
  description = "az de região D publica"
}

variable "CIDR_main_DMZ_D" {
  type        = string
  description = "range de rede subnet D publica"
}

variable "main_APP_A" {
  type        = string
  description = "az de região A privada"
}

variable "CIDR_main_APP_A" {
  type        = string
  description = "range de rede subnet A privada"
}

variable "main_APP_B" {
  type        = string
  description = "az de região B privada"
}

variable "CIDR_main_APP_B" {
  type        = string
  description = "range de rede subnet B privada"
}

variable "main_APP_C" {
  type        = string
  description = "az de região C privada"
}

variable "CIDR_main_APP_C" {
  type        = string
  description = "range de rede subnet C privada"
}

variable "main_APP_D" {
  type        = string
  description = "az de região D privada"
}

variable "CIDR_main_APP_D" {
  type        = string
  description = "range de rede subnet D privada"
}

variable "instance_tenancy" {
  type        = string
  description = "Tenancy network"
}

variable "enable_dns_support" {
  type        = bool
  description = "Habilita o DNS da vpc"
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "Habilita conexão entre hosts e DNS da vpc"
}

variable "enable_classiclink" {
  type        = bool
  description = "Habilota classick link"
}

variable "name_vpc" {
  type        = string
  description = "Nome da VPC"
}

variable "env" {
  type        = string
  description = "Ambiente"
}

variable "cluster-name" {
  type        = string
  description = "Nome do cluster"
}